document.getElementById("Title Asc").addEventListener("click", displayTitleAsc);
document.getElementById("Title Desc").addEventListener("click", displayTitleDesc);
document.getElementById("Recent").addEventListener("click", displayRecent);
document.getElementById("Old").addEventListener("click", displayOld);
document.getElementById("delAll").addEventListener("click", delAll);
document.getElementById("sport").addEventListener("click", launchSport);

let id;
let idNotif;

function launchSport() {
    document.location.href = "sport.html";
}

function displayTitleAsc() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(queryDBTitleAsc, errorDB);
}

function displayTitleDesc() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(queryDBTitleDesc, errorDB);
}

function displayRecent() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(queryDBRecent, errorDB);
}

function displayOld() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(queryDBOld, errorDB);
}

function delAll() {
    let sure = confirm("Delete all tasks ?");
    if (sure) {
        let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
        db.transaction(delAllDB, errorDB, successDB);
        document.location.href = "index.html";
    }
}

function queryDBTitleAsc(tx) {
    tx.executeSql('SELECT * FROM TODO order by title ASC', [], querySuccessDisplay, errorDB);
}

function queryDBTitleDesc(tx) {
    tx.executeSql('SELECT * FROM TODO order by title DESC', [], querySuccessDisplay, errorDB);
}

function queryDBRecent(tx) {
    tx.executeSql('SELECT * FROM TODO order by date DESC, time DESC ', [], querySuccessDisplay, errorDB);
}

function queryDBOld(tx) {
    tx.executeSql('SELECT * FROM TODO order by date ASC, time ASC ', [], querySuccessDisplay, errorDB);
}

function delAllDB(tx) {
    tx.executeSql('DELETE FROM TODO');
}

function querySuccessDisplay(tx, results) {
    let len = results.rows.length;
    console.log("DISPLAY: TODO table: " + len + " rows found.");
    document.getElementById('div').innerHTML = '';
    for (let i = 0; i < len; i++) {
        let output;
        output =
            'Title : <span class="bold">' + results.rows.item(i).title + "</span>" +
            '<br> Description : <span class="bold">' + results.rows.item(i).description + "</span>" +
            '<br> Type : <span class="bold">' + results.rows.item(i).type + "</span>" +
            '<br> Notification : <span class="bold">' + results.rows.item(i).notification + "</span>" +
            '<br> Location : <span class="bold">' + results.rows.item(i).location.split("/")[0] + "</span>" +
            '<br> Latitude : <span class="bold">' + results.rows.item(i).location.split("/")[1] + "</span>" +
            '<br> Longitude : <span class="bold">' + results.rows.item(i).location.split("/")[2] + "</span>" +
            '<br> Date : <span class="bold">' + results.rows.item(i).date + "</span>" +
            '<br> Time : <span class="bold">' + results.rows.item(i).time + "</span>" +
            '<div class="buttonGroup">' +
            '<button class="Notify" id="NotifySPLITHERE' + results.rows.item(i).id + '">Notify</button>' +
            '<button class="Edit" id="EditSPLITHERE' + results.rows.item(i).id + '">Edit</button>' +
            '<button class="Delete" id="DeleteSPLITHERE' + results.rows.item(i).id + '">Delete</button>' +
            '</div>'
        ;
        console.log(output);
        document.getElementById('div').innerHTML += '<div class="task">' + output + '</div>';
    }
    listenerAttach();
}

function edit(id) {
    console.log("edit id: " + id);
    document.location.href = "edit.html?id=" + id;
}

function deleteDB(id) {
    let sure = confirm("Delete this task ?");
    if (sure) {
        console.log("deleting id: " + id);
        this.id = id;
        let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
        db.transaction(deleteDBProcess, errorDB);
    }
}

function deleteDBProcess(tx) {
    console.log("id got: " + this.id);
    tx.executeSql('DELETE FROM TODO WHERE ID = ' + this.id);
    displayTitleAsc();
}

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(createDB, errorDB, successDB);
    db.transaction(queryDB, errorDB);
}

function createDB(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS TODO (id INTEGER NOT NULL PRIMARY KEY, title varchar(150), description varchar(150), type varchar(150), notification varchar(150), location varchar(150), date varchar(150), time varchar(150))');
    tx.executeSql('CREATE TABLE IF NOT EXISTS SPORT (id INTEGER NOT NULL PRIMARY KEY, date varchar(150), value varchar(150))');
}

function errorDB(tx, err) {
    alert("Error processing SQL: " + err);
}

function successDB() {
    console.log("Database success !");
}

function queryDB(tx) {
    tx.executeSql('SELECT * FROM TODO', [], querySuccess, errorDB);
}

function querySuccess(tx, results) {
    let len = results.rows.length;
    console.log("TODO table: " + len + " rows found.");
    for (let i = 0; i < len; i++) {
        console.log(
            "Row = " + i +
            ", ID = " + results.rows.item(i).id +
            ", TITLE = " + results.rows.item(i).title +
            ", DESCRIPTION = " + results.rows.item(i).description +
            ", TYPE = " + results.rows.item(i).type +
            ", NOTIFICATION = " + results.rows.item(i).notification +
            ", LOCATION = " + results.rows.item(i).location +
            ", DATE = " + results.rows.item(i).date +
            ", TIME = " + results.rows.item(i).time
        );
    }
}

function listenerAttach() {
    document.querySelectorAll('div[class="buttonGroup"]').forEach(function (element) {
        element.querySelectorAll('button').forEach(function (el) {
            el.addEventListener('click', function () {
                let id = el.getAttribute("id").split("SPLITHERE");
                console.log(id);
                if (id[0] === "Delete") {
                    deleteDB(id[1]);
                }
                if (id[0] === "Edit") {
                    edit(id[1]);
                }
                if (id[0] === "Notify") {
                    notification(id[1]);
                }
            });
        });
    });
}

function notification(id) {
    this.idNotif = id;
    console.log("this.idNotif = " + this.idNotif);
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(getTupleNotif, errorDB);
}

function getTupleNotif(tx) {
    tx.executeSql('SELECT * FROM TODO WHERE ID = ' + this.idNotif, [], getTupleNotifSuccess, errorDB);
}

function getTupleNotifSuccess(tx, results) {
    let len = results.rows.length;
    console.log("getTupleNotifSuccess table: " + len + " rows found.");
    for (let i = 0; i < len; i++) {
        console.log(
            "Row = " + i +
            ", ID = " + results.rows.item(i).id +
            ", TITLE = " + results.rows.item(i).title +
            ", DESCRIPTION = " + results.rows.item(i).description +
            ", TYPE = " + results.rows.item(i).type +
            ", NOTIFICATION = " + results.rows.item(i).notification +
            ", LOCATION = " + results.rows.item(i).location +
            ", DATE = " + results.rows.item(i).date +
            ", TIME = " + results.rows.item(i).time
        );
    }

    if (results.rows.item(0).type !== "Athletic") {
        window.confirm("NOTIFICATION\n\nTitle : " + results.rows.item(0).title +
            "\nDescription : " + results.rows.item(0).description +
            "\nType : " + results.rows.item(0).type +
            "\nLatitude : " + results.rows.item(0).location.split("/")[1] +
            "\nLongitude : " + results.rows.item(0).location.split("/")[2] +
            "\nDate : " + results.rows.item(0).date +
            "\nTime : " + results.rows.item(0).time
        );
    } else {
        let sport = window.confirm("ATHLETIC NOTIFICATION\n\nTitle : " + results.rows.item(0).title +
            "\nDescription : " + results.rows.item(0).description +
            "\nType : " + results.rows.item(0).type +
            "\nDate : " + results.rows.item(0).date +
            "\nTime : " + results.rows.item(0).time +
            "\n\nDo you want to start a sport activity ?"
        );
        if (sport) {
            document.location.href = "sport.html";
        }
    }
}

/*

cordova run browser -- --live-reload

PROBLEM GEOLOCALISATION SUR ANDROID => regler wifi, puis revenir sur app

*/
