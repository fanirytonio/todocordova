document.getElementById("todo").addEventListener("click", launchTodo);
document.getElementById("Cancel").addEventListener("click", cancel);
document.getElementById("Save").addEventListener("click", save);
document.getElementById("Best").addEventListener("click", best);
document.getElementById("Worst").addEventListener("click", worst);
document.getElementById("DeleteAll").addEventListener("click", deleteAll);

let id;
let rowsNumber;

function deleteAll() {
    let sure = confirm("Delete all records ?");
    if (sure) {
        let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
        db.transaction(delAllDB, errorDB, successDB);
        document.location.href = "sport.html";
    }
}

function delAllDB(tx) {
    tx.executeSql('DROP TABLE SPORT');
    tx.executeSql('CREATE TABLE IF NOT EXISTS SPORT (id INTEGER NOT NULL PRIMARY KEY, date varchar(150), value varchar(150))');
}

function best() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(queryDBBest, errorDB);
}

function worst() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(queryDBWorst, errorDB);
}

function queryDBBest(tx) {
    tx.executeSql('SELECT * FROM SPORT order by value DESC', [], querySuccessDisplay, errorDB);
}

function queryDBWorst(tx) {
    tx.executeSql('SELECT * FROM SPORT order by value ASC', [], querySuccessDisplay, errorDB);
}

function querySuccessDisplay(tx, results) {
    let len = results.rows.length;
    console.log("SPORT: SPORT table: " + len + " rows found.");
    document.getElementById('div').innerHTML = '';
    for (let i = 0; i < len; i++) {
        let output;
        output =
            'Date : <span class="bold">' + results.rows.item(i).date + "</span>" +
            '<br> Record : <span class="bold">' + results.rows.item(i).value + "</span>" +
            '<div class="buttonGroup">' +
            '<button class="Delete" id="DeleteSPLITHERE' + results.rows.item(i).id + '">Delete</button>' +
            '</div>'
        ;
        console.log(output);
        document.getElementById('div').innerHTML += '<div class="task">' + output + '</div>';
    }
    listenerAttach();
}

function listenerAttach() {
    document.querySelectorAll('div[class="buttonGroup"]').forEach(function (element) {
        element.querySelectorAll('button').forEach(function (el) {
            el.addEventListener('click', function () {
                let id = el.getAttribute("id").split("SPLITHERE");
                console.log(id);
                if (id[0] === "Delete") {
                    deleteDB(id[1]);
                }
            });
        });
    });
}

document.addEventListener("deviceready", onDeviceReady, false);

function launchTodo() {
    document.location.href = "index.html";
}

function cancel() {
    document.getElementById('val').innerHTML = 0;
}

function save() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(queryDB, errorDB);
    db.transaction(insertDB, successDB);
}

function onDeviceReady() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(createDB, errorDBcreateDB, successDB);
    db.transaction(queryDB, errorDB);
    navigator.accelerometer.getCurrentAcceleration(onSuccess, onError);

    let options = {frequency: 1000};  // Update every 3 seconds
    navigator.accelerometer.watchAcceleration(onSuccess, onError, options);
}

function onSuccess(acceleration) {
    console.log('Acceleration X: ' + acceleration.x + '\n' +
        'Acceleration Y: ' + acceleration.y + '\n' +
        'Acceleration Z: ' + acceleration.z + '\n');
    let max = Math.max(Math.abs(acceleration.x), Math.abs(acceleration.y), Math.abs(acceleration.z));
    console.log(max);

    let currentVal = document.getElementById('val').innerText;

    if (currentVal<max) {
        document.getElementById('val').innerHTML = max.toFixed(5);
    }
}

function onError() {
    console.log('onError accelerometer !');
}

function createDB(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS SPORT (id INTEGER NOT NULL PRIMARY KEY, date varchar(150), value varchar(150))');
}

function successDB() {
    console.log("Database success !");
}

function queryDB(tx) {
    tx.executeSql('SELECT * FROM SPORT', [], querySuccess, errorDB);
}

function querySuccess(tx, results) {
    let len = results.rows.length;
    console.log("SPORT table: " + len + " rows found.");
    for (let i = 0; i < len; i++) {
        console.log(
            "Row = " + i +
            ", ID = " + results.rows.item(i).id +
            ", DATE = " + results.rows.item(i).date +
            ", VALUE = " + results.rows.item(i).value
        );
    }
    this.rowsNumber = len * 2 + 3;
}

function insertDB(tx) {
    let date, value;

    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;//January is 0, so always add + 1
    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    let currentdate = new Date();
    let time = currentdate.getHours() + ":" + currentdate.getMinutes();

    date = today + " at " + time;
    value = document.getElementById("val").innerText;

    let id = currentdate.getSeconds() + currentdate.getMilliseconds() + 3;

    let query = 'INSERT INTO SPORT (id, date, value)' +
        ' VALUES (' + id + ', ' + '"' + date + '"' + ', ' + '"' + value + '")';

    console.log(query);

    tx.executeSql(query, [], insertSuccess);

    window.plugins.toast.showWithOptions({
        message: "Record added !",
        duration: "short",
        position: "center",
        styling: {
            opacity: 0.75, // Default 0.8
            backgroundColor: '#9fdf9f',// Default #333333
            textColor: '#000000',// Default #FFFFFF
            textSize: 20.5, // Default 13
            cornerRadius: 25, // iOS default 20, Android Default 100 and minimum is 0
            horizontalPadding: 20, // iOS default 16 and Android Default 50
            verticalPadding: 16 // iOS default 12 and Android Default 30
        }
    });
}

function insertSuccess() {
    console.log("insert successed !");
}

function deleteDB(id) {
    console.log("deleting id: " + id);
    this.id = id;
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(deleteDBProcess, errorDB);
}

function deleteDBProcess(tx) {
    console.log("id got: " + this.id);
    tx.executeSql('DELETE FROM SPORT WHERE ID = ' + this.id);
    displayTitleAsc();
}

function displayTitleAsc() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(queryDBTitleAsc, errorDB);
}

function queryDBTitleAsc(tx) {
    tx.executeSql('SELECT * FROM SPORT order by value DESC', [], querySuccessDisplay, errorDB);
}

function errorDB(tx, err) {
    alert("Error processing SQL: " + err);
}

function errorDBcreateDB(tx, err) {
    alert("Error createDB processing SQL: " + err);
}