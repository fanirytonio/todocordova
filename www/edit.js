let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
db.transaction(getDB, errorDB);
console.log(document.URL.toString().split("=")[1]);

document.getElementById("geoLocation").addEventListener("click", geoLoc);


let onSuccess = function (position) {
    let lat = position.coords.latitude, long = position.coords.longitude;
    console.log('Latitude : ' + lat + '\n' +
        'Longitude : ' + long);
    document.getElementById('lat').innerHTML = lat;
    document.getElementById('long').innerHTML = long;
};

function onError(error) {
    console.log('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

function geoLoc() {
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
}

document.getElementById("myButton").addEventListener("click", myFunction);

function getDB(tx) {
    tx.executeSql('SELECT * FROM TODO WHERE ID = ' + document.URL.toString().split("=")[1], [], querySuccess, errorDB);
}

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(createDB, errorDB, successDB);
}

function queryDB(tx) {
    tx.executeSql('SELECT * FROM TODO', [], querySuccessID, errorDB);
}

function querySuccessID(tx, results) {
    let len = results.rows.length;
    console.log("TODO table: " + len + " rows found.");
    this.rowsNumber = len * 2 + 3;
}

function querySuccess(tx, results) {
    let len = results.rows.length;
    console.log("TODO table: " + len + " rows found.");
    for (let i = 0; i < len; i++) {
        console.log(
            "Row = " + i +
            ", ID = " + results.rows.item(i).id +
            ", TITLE = " + results.rows.item(i).title +
            ", DESCRIPTION = " + results.rows.item(i).description +
            ", TYPE = " + results.rows.item(i).type +
            ", NOTIFICATION = " + results.rows.item(i).notification +
            ", LOCATION = " + results.rows.item(i).location +
            ", DATE = " + results.rows.item(i).date +
            ", TIME = " + results.rows.item(i).time
        );
        document.getElementById('title').value = results.rows.item(i).title;
        document.getElementById('description').value = results.rows.item(i).description;
        document.getElementById('type').value = results.rows.item(i).type;

        if (results.rows.item(i).notification === 'YES') {
            document.getElementById("notification").checked = true;
        } else document.getElementById("notification").checked = false;

        document.getElementById("lat").innerText = results.rows.item(i).location.split("/")[1];
        document.getElementById("long").innerText = results.rows.item(i).location.split("/")[2];

        document.getElementById('date').value = results.rows.item(i).date;
        document.getElementById('time').value = results.rows.item(i).time;
    }
}

function createDB(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS TODO (id INTEGER NOT NULL PRIMARY KEY, title varchar(150), description varchar(150), type varchar(150), notification varchar(150), location varchar(150), date varchar(150), time varchar(150))');
}

function errorDB(tx, err) {
    alert("Error processing SQL: " + err);
}

function successDB() {
    console.log("Database success !");
}

function myFunction() {

    let titleTodo = document.getElementById("title").value;
    let descriptionTodo = document.getElementById("description").value;

    if (titleTodo === '' || descriptionTodo === '') {
        alert("Title and Description are required !");
    } else {
        console.log("ici");
        updateDB();
    }
}

function updateDB() {
    let id = document.URL.toString().split("=")[1];
    console.log("updating id: " + id);
    let db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
    db.transaction(updateDBProcess, errorDB);
}

function updateDBProcess(tx) {
    let notificationTodo, locationTodo;

    let titleTodo = document.getElementById("title").value;
    let descriptionTodo = document.getElementById("description").value;
    let typeTodo = document.getElementById("type").value;
    let dateTodo = document.getElementById("date").value;
    let timeTodo = document.getElementById("time").value;

    if (document.getElementById("notification").checked === true) {
        notificationTodo = "YES";
    } else notificationTodo = "NO";

    let lat = document.getElementById("lat").innerText;
    let long = document.getElementById("long").innerText;

    if (lat === "None" || long === "None") {
        locationTodo = "NO/None/None";
    } else locationTodo = "YES/" + lat + "/" + long;

    if (dateTodo === '') {
        dateTodo = "2019-01-31";
    }


    console.log("id got: " + document.URL.toString().split("=")[1]);

    let query = 'UPDATE TODO SET title = ' + '"' + titleTodo + '", description = ' + '"' +
        descriptionTodo + '", type = ' + '"' + typeTodo + '", notification = ' + '"' + notificationTodo + '", location = ' + '"' +
        locationTodo + '", date = ' + '"' + dateTodo + '", time = ' + '"' + timeTodo + '" WHERE id = ' + document.URL.toString().split("=")[1];
    console.log(query);

    tx.executeSql(query);

    window.plugins.toast.showWithOptions({
        message: "Task modified !",
        duration: "short",
        position: "center",
        styling: {
            opacity: 0.75, // Default 0.8
            backgroundColor: '#9fdf9f',// Default #333333
            textColor: '#000000',// Default #FFFFFF
            textSize: 20.5, // Default 13
            cornerRadius: 25, // iOS default 20, Android Default 100 and minimum is 0
            horizontalPadding: 20, // iOS default 16 and Android Default 50
            verticalPadding: 16 // iOS default 12 and Android Default 30
        }
    });
}